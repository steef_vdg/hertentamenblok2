# README #

Hoe uitchecken?

a. Maak een Fork van het project aan in jouw eigen account. Zo ontstaat een kopie van de repository op jouw eigen bitbucket omgeving 

b. Maak binnen die fork een eigen branch aan. Zorg dat de naam van jouw branch overeenkomt met jouw studentnummer met toevoegsel: KANS1.
 
c. Clone de fork op je eigen werkomgeving op je laptop 

d. voeg antonbil toe als gebruiker van de fork, met schrijfrechten 

e. Zorg dat jouw gegevens (naam en persoonlijke Bitbucket URL) zijn toegevoegd aan het bestand authors.md in de projectfolder Let er op dat je ieder NIEUW bestand dat je maakt via git add toevoegt aan je repository!


Hoe inleveren?

Lever je uitwerking op door middel van een pull-request. In dit pull request verzoek je een merge van jouw branch met de master-branch van antonbil.

Tip: denk ook aan het toevoegen van comments aan je code!